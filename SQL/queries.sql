CREATE DATABASE my_training_by CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci;

CREATE TABLE users
(
    user_id      INT AUTO_INCREMENT          NOT NULL,
    name         VARCHAR(100),
    surname      VARCHAR(100),
    login        VARCHAR(100)                NOT NULL,
    password     VARCHAR(100)                NOT NULL,
    role         ENUM ('student', 'trainer') NOT NULL,
    email        VARCHAR(100),
    phone_number VARCHAR(20),
    PRIMARY KEY (user_id)
);

CREATE TABLE tasks
(
    task_id    INT AUTO_INCREMENT NOT NULL,
    title      varchar(255)       NOT NULL,
    text       VARCHAR(2000),
    start_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    deadline   DATETIME,
    creator_id INT,
    PRIMARY KEY (task_id),
    FOREIGN KEY (creator_id) REFERENCES users (user_id)
);

CREATE TABLE solutions
(
    solution_id     INT AUTO_INCREMENT NOT NULL,
    student_id      INT                NOT NULL,
    task_id         INT                NOT NULL,
    submission_time DATETIME DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
    content         VARCHAR(255),
    grade           DOUBLE,
    PRIMARY KEY (solution_id),
    FOREIGN KEY (student_id) REFERENCES users (user_id),
    FOREIGN KEY (task_id) REFERENCES tasks (task_id)
);