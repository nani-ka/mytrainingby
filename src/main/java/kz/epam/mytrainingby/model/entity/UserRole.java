package kz.epam.mytrainingby.model.entity;

public enum UserRole {
    STUDENT,
    TRAINER
}
