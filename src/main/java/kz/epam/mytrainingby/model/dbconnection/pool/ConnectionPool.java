package kz.epam.mytrainingby.model.dbconnection.pool;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingDeque;

public enum ConnectionPool {
    INSTANCE;

    //removed 'static' becaues couldn't use the logger inside the constructor
    //there is only one instance, so the same result as with 'static'
    private final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);

    private BlockingQueue<Connection> freeConnections;
    private Queue<Connection> givenConnections;
    private static final int DEFAULT_POOL_SIZE = 32;

    ConnectionPool() {
        Properties props = new Properties();
        freeConnections = new LinkedBlockingDeque<>();
        givenConnections = new ArrayDeque<>();

        try {
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver());
            props.load(new FileInputStream("src/main/resources/dbparams.properties"));
        } catch (SQLException e) {
            LOGGER.error("SQL Error: " + e);
        } catch (FileNotFoundException e) {
            LOGGER.error("File Error: " + e);
        } catch (IOException e) {
            LOGGER.error("I/O Stream Error: " + e);
        }

        for (int i = 0; i < DEFAULT_POOL_SIZE; i++) {
            try {
                freeConnections.offer(DriverManager.getConnection(props.getProperty("url"), props));
            } catch (SQLException e) {
                LOGGER.error("SQL Error: " + e);
            }
        }
    }

    public Connection getConnection() {
        Connection connection = null;
        try {
            connection = freeConnections.take();
            givenConnections.offer(connection);
        } catch (InterruptedException e) {
            LOGGER.error("Interruption Error: " + e);
        }
        return connection;
    }

    public void releaseConnection(Connection connection) {
        if (connection.getClass() == ProxyConnection.class) {
            givenConnections.remove(connection);
            freeConnections.offer(connection);
        } else {
            LOGGER.warn("Attempt to release a wild connection.");
        }
    }

    public void destroyPool() {
        for (int i = 0; i < DEFAULT_POOL_SIZE; i++) {
            try {
                ((ProxyConnection) freeConnections.take()).reallyClose();
            } catch (InterruptedException e) {
                LOGGER.error("Interruption Error: " + e);
            }
        }
        deregisterDrivers();
    }

    private void deregisterDrivers() {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            try {
                DriverManager.deregisterDriver(drivers.nextElement());
            } catch (SQLException e) {
                LOGGER.error("SQL Error: " + e);
            }
        }
    }
}
